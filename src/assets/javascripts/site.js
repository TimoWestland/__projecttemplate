// This website uses pollyfills to apply the forward compatiblity
// development principle. These should be loaded before all libraries
// in order to enable features before using them.
require('./polyfill/Array.forEach');
require('./polyfill/Array.indexOf');
require('./polyfill/Array.map');
require('./polyfill/Element.classList');
require('./polyfill/EventTarget.addEventListener');
require('./polyfill/Function.bind');
require('./polyfill/Input.placeholder');
require('./polyfill/NodeList.toArray');
require('./polyfill/NodeList.forEach');
require('./polyfill/Object.assign');
require('./polyfill/Object.create');
require('./polyfill/String.trim');
require('./polyfill/Window.getComputedStyle');



document.addEventListener('DOMContentLoaded', function(){
    // Layout trashing is a big issue during the initial load of the website
    // In order to prevent this fastdom is used to synchronize reads and writes
    // from and to the dom.
    var fastdom = require('fastdom');

    // Initialize Menu, this is done first thing since its the first
    // object a user sees on the page
    fastdom.read(function(){
        var menuElement = document.querySelector('.js-el-menu');

        if (menuElement) {
            var Menu = require('./component/Menu');
            new Menu(menuElement);
        }
    });
});