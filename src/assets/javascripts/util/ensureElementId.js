var uniqueElementId = require('./uniqueElementId.js');

module.exports = function(element){
	var elementId = element.id;

	if ( !elementId ) {
		element.id = elementId = uniqueElementId();
	}

	return elementId;
};
