/**
 * Forces the page to recalculate viewport measurements. By
 * resetting the content attribute of the viewport element.
 */

/**
 * Reference to viewport element on page
 */
var viewportElement;

/**
 * Original value of vieport element content attribute
 */
var viewportContent;

/**
 * Detects the viewport element and value of content attribute
 */
var detectViewportElement = function(){
    if (!viewportElement) {
        viewportElement = document.querySelector('meta[name="viewport"]');
        viewportContent = viewportElement.getAttribute('content');
    }
};


var redrawViewportTimeout;

/**
 * Redraws viewport by resetting content tag and forcing
 * layout change on body element
 */
var redrawViewport = function(){
    detectViewportElement();

    if ( viewportElement ){
        clearTimeout(redrawViewportTimeout);

        document.body.style.opacity = 0;
        viewportElement.setAttribute('content', '');

        redrawViewportTimeout = setTimeout(function(){
            viewportElement.setAttribute('content', viewportContent);
            document.body.style.opacity = '';
        }, 100);
    }
};

module.exports = redrawViewport;
