var nodeListForEach = function(){
    var items = this.toArray();
    return items.forEach.apply(items, arguments);
};

if ( window.NodeList && NodeList.prototype && !NodeList.prototype.forEach ){
    NodeList.prototype.forEach = nodeListForEach;
}

if ( window.StaticNodeList && StaticNodeList.prototype && !StaticNodeList.prototype.forEach ){
    StaticNodeList.prototype.forEach = nodeListForEach;
}
